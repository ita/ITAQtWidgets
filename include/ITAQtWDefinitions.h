/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_DEFINITIONS
#define IW_ITA_QT_WIDGETS_DEFINITIONS

#if ( defined WIN32 ) && !( defined ITA_QT_WIDGETS_STATIC )
 #ifdef ITA_QT_WIDGETS_EXPORT
  #define ITA_QT_WIDGETS_API __declspec( dllexport )
 #else
  #define ITA_QT_WIDGETS_API __declspec( dllimport )
 #endif
#else
 #define ITA_QT_WIDGETS_API
#endif

#endif // IW_ITA_QT_WIDGETS_DEFINITIONS
