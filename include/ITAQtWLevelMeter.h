/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_LEVEL_METER
#define IW_ITA_QT_WIDGETS_LEVEL_METER

#include <ITAQtWDefinitions.h>

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QGraphicsLineItem>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QScrollBar>

#include <ITAException.h>

#include <cmath>
#include <iostream>
#include <vector>

class ITA_QT_WIDGETS_API CITAQtWLevelMeter : public QGraphicsView
{
	Q_OBJECT

public:
	//! Level meter widget
	CITAQtWLevelMeter( QWidget* parent = Q_NULLPTR );
	~CITAQtWLevelMeter();

	void SetNumChannels( const int iNumChannels );
	int GetNumChannels() const;
	
	//! If update on level change is activated, the setters will trigger an update. This can be resource intensive.
	void UpdateOnLevelChange( bool bUpate );

	void SetPeak( const int iChannel, const float fFactor );
	void SetPeaks( const std::vector< float >& vfFactors );
	void SetRMS( const int iChannel, const float fRMS );
	void SetRMSs( const std::vector< float >& vfRMSs );
	void SetSensitivity( const int iChannel, const float fSensitivity );
	void SetSensitivities( const std::vector< float >& vfSensitivity );
	
private slots:
	void UpdateLevels();

signals:
	void LevelMetersChanged();

private:
	std::vector< float > m_vfPeaks; //!< Factors with level values (linear)
	std::vector< float > m_vfRMSs; //!< Factors with root-mean-square (RMS) values (linear)
	std::vector< float > m_vfSensitivities; //!< Sensitivities factor (linear)
	std::vector< QGraphicsLineItem > m_voLines; //!< Graphics line items of levels
	double m_dDynamicRangeDB; //! Dynamic range
	double m_dMaximumLevelDB;
	bool m_bUpdateOnLevelChange;
};

#endif // IW_ITA_QT_WIDGETS_LEVEL_METER
