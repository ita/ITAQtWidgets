/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <QApplication>
#include <QMainWindow>
#include "ITAQtWTestWindow.h"
#include <ui_ITAQtWTestWindow.h>

ITAQtWTestWindow::ITAQtWTestWindow( QWidget *parent )
	: QMainWindow( parent )
	, ui( new Ui::ITAQtWTestWindow )
{
	ui->setupUi( this );
};

ITAQtWTestWindow::~ITAQtWTestWindow()
{
	delete ui;
};

void ITAQtWTestWindow::on_actionQuit_triggered()
{
	close();
};


int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	a.setOrganizationName( "Institute of Technical Acoustics" );
	a.setOrganizationDomain( "akustik.rwth-aachen.de" );
	a.setApplicationName( "ITAQtWTestWindow" );
	a.setApplicationDisplayName( "ITAQtWidgets" );

	ITAQtWTestWindow w;
	w.show();

	return a.exec();
};
