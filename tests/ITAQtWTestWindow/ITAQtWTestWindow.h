/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_TEST_WINDOW
#define IW_ITA_QT_WIDGETS_TEST_WINDOW

#include <QMainWindow>

namespace Ui
{
	class ITAQtWTestWindow;
}

class ITAQtWTestWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit ITAQtWTestWindow( QWidget *parent = 0 );
	~ITAQtWTestWindow();

private slots:

    void on_actionQuit_triggered();

private:
	Ui::ITAQtWTestWindow* ui;
};

#endif // IW_ITA_QT_WIDGETS_TEST_WINDOW