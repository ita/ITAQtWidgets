/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <QApplication>
#include <QMainWindow>
#include "ITAQtWGraphicsViewTestWindow.h"

int main( int argc, char *argv[] )
{
	QApplication a( argc, argv );
	a.setOrganizationName( "Institute of Technical Acoustics" );
	a.setOrganizationDomain( "akustik.rwth-aachen.de" );
	a.setApplicationName( "ITAQtWTestWindow" );
	a.setApplicationDisplayName( "ITAQtWidgets" );

	ITAQtWGraphicsViewTestWindow w;
	w.show();

	return a.exec();
};
