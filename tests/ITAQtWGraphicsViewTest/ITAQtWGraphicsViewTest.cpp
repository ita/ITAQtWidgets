/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include "ITAQtWGraphicsViewTest.h"

CITAQtWGraphicsViewTest::CITAQtWGraphicsViewTest( QWidget* parent /* = 0 */ )
	: QGraphicsView( parent )
{
}

CITAQtWGraphicsViewTest::~CITAQtWGraphicsViewTest()
{

}
