/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST_WINDOW
#define IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST_WINDOW

#include <QMainWindow>

namespace Ui
{
	class ITAQtWGraphicsViewTestWindow;
}

class ITAQtWGraphicsViewTestWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit ITAQtWGraphicsViewTestWindow( QWidget *parent = 0 );
	~ITAQtWGraphicsViewTestWindow();

private slots:

    void on_actionQuit_triggered();

private:
	Ui::ITAQtWGraphicsViewTestWindow* ui;
};

#endif // IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST_WINDOW