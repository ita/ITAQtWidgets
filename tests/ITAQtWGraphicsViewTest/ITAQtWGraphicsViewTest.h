/*
 * ----------------------------------------------------------------
 *
 *		ITAMess measurement application
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST
#define IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST

#include <QGraphicsView>

class CITAQtWGraphicsViewTest : public QGraphicsView
{
	Q_OBJECT

public:
	explicit CITAQtWGraphicsViewTest( QWidget* parent = 0 );
	~CITAQtWGraphicsViewTest();

private slots:


private:

};

#endif // IW_ITA_QT_WIDGETS_GRAPHICS_VIEW_TEST