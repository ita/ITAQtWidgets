/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include "ITAQtWGraphicsViewTestWindow.h"
#include <ui_ITAQtWGraphicsViewTestWindow.h>

ITAQtWGraphicsViewTestWindow::ITAQtWGraphicsViewTestWindow( QWidget *parent )
	: QMainWindow( parent )
	, ui( new Ui::ITAQtWGraphicsViewTestWindow )
{
	ui->setupUi( this );
}

ITAQtWGraphicsViewTestWindow::~ITAQtWGraphicsViewTestWindow()
{
}

void ITAQtWGraphicsViewTestWindow::on_actionQuit_triggered()
{
	close();
}
