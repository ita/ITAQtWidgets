/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <QApplication>
#include "ITAQtWLevelMeterTestWindow.h"

#include <ITAPortaudioInterface.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamDetector.h>
#include <ITAStreamProbe.h>
#include <ITAQtWLevelMeter.h>

#include <iostream>


int main( int argc, char *argv[] )
{
	double dSampleRate = 44.1e3;
	int iBlockLength = 256;

	// Recording
	ITAPortaudioInterface ITAPA_IN( dSampleRate, iBlockLength );
	int iInputDevice = ITAPA_IN.GetDefaultInputDevice();;
	ITAPA_IN.Initialize( iInputDevice );
	std::string sInDevice = ITAPA_IN.GetDeviceName( iInputDevice );
	std::cout << sInDevice << std::endl;
	int iNumInputChannels, temp;
	ITAPA_IN.GetNumChannels( iInputDevice, iNumInputChannels, temp );

	ITAPA_IN.SetPlaybackEnabled( false );
	ITAPA_IN.SetRecordEnabled( true );
	ITAStreamProbe oRecordStreamProbe( ITAPA_IN.GetRecordDatasource(), "ITAQtWLevelMeterTest_Record.wav" );
	ITAStreamDetector oRecordDetector( &oRecordStreamProbe );
	
	ITAPA_IN.Open();
	ITAPA_IN.Start();

	// Playback
	ITAPortaudioInterface ITAPA_OUT( dSampleRate, iBlockLength );
	int iOutputDevice = ITAPA_OUT.GetDefaultOutputDevice();;
	ITAPA_OUT.Initialize( iOutputDevice );
	std::string sOutDevice = ITAPA_OUT.GetDeviceName( iOutputDevice );
	std::cout << sOutDevice << std::endl;
	int iNumOutputChannels;
	ITAPA_OUT.GetNumChannels( iOutputDevice, temp, iNumOutputChannels );

	ITAStreamFunctionGenerator oPlayback( iNumOutputChannels, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 377.0f, 0.0f, true );
	ITAStreamProbe oPlaybackStreamProbe( &oPlayback, "ITAQtWLevelMeterTest_Playback.wav" );
	ITAStreamDetector oPlaybackDetector( &oPlaybackStreamProbe );

	// Start streaming
	ITAPA_OUT.SetRecordEnabled( false );
	ITAPA_OUT.SetPlaybackEnabled( true );
	ITAPA_OUT.SetPlaybackDatasource( &oPlaybackDetector );

	ITAPA_OUT.Open();
	ITAPA_OUT.Start();

	// Qt app
	QApplication a( argc, argv );
	a.setOrganizationName( "Institute of Technical Acoustics" );
	a.setOrganizationDomain( "akustik.rwth-aachen.de" );
	a.setApplicationName( "ITAQtWTestWindow" );
	a.setApplicationDisplayName( "ITAQtWidgets" );

	CITAQtWLevelMeterTestWindow w;
	w.SetStreamDetector( &oRecordDetector );
	w.show();
	
	int iRet = a.exec();

	// Stop streaming
	ITAPA_OUT.Stop();
	ITAPA_OUT.Close();
	ITAPA_OUT.Finalize();

	ITAPA_IN.Stop();
	ITAPA_IN.Close();
	ITAPA_IN.Finalize();

	return iRet;
};
