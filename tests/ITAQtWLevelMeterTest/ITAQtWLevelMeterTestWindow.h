/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_QT_WIDGETS_LEVEL_METER_TEST_WINDOW
#define IW_ITA_QT_WIDGETS_LEVEL_METER_TEST_WINDOW

#include <QMainWindow>
#include <QTimer>

#include <ITAQtWLevelMeter.h>

#include <ITAStreamInfo.h>

class ITAStreamDetector;

namespace Ui
{
	class ITAQtWLevelMeterTestWindow;
}

class CITAQtWLevelMeterTestWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit CITAQtWLevelMeterTestWindow( QWidget *parent = 0 );
	~CITAQtWLevelMeterTestWindow();
	void SetStreamDetector( ITAStreamDetector* );

private slots:
	void Render();
	void ProcessAudioStream();
    void on_actionQuit_triggered();

private:
	Ui::ITAQtWLevelMeterTestWindow* ui;
	ITAStreamDetector* m_pDetector;
	QTimer m_qRenderTimer, m_qAudioProcessTimer;
	std::vector< float > m_vfPeaks;
	std::vector< float > m_vfRMSs;
	ITAStreamInfo m_siState;
};

#endif // IW_ITA_QT_WIDGETS_LEVEL_METER_TEST_WINDOW
