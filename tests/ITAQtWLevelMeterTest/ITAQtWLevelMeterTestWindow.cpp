/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include "ITAQtWLevelMeterTestWindow.h"
#include <ui_ITAQtWLevelMeterTestWindow.h>

#include <ITAStreamDetector.h>
#include <ITAAudiofileCommon.h>
#include <ITAAudiofileWriter.h>

CITAQtWLevelMeterTestWindow::CITAQtWLevelMeterTestWindow( QWidget *parent )
	: QMainWindow( parent )
	, ui( new Ui::ITAQtWLevelMeterTestWindow )
	, m_pDetector( NULL )
{
	ui->setupUi( this );

	connect( &m_qRenderTimer, SIGNAL( timeout() ), this, SLOT( Render() ) );
	connect( &m_qAudioProcessTimer, SIGNAL( timeout() ), this, SLOT( ProcessAudioStream() ) );
}

CITAQtWLevelMeterTestWindow::~CITAQtWLevelMeterTestWindow()
{
	if( m_qRenderTimer.isActive() )
		m_qRenderTimer.stop();

	if( m_qAudioProcessTimer.isActive() )
		m_qAudioProcessTimer.stop();

}

void CITAQtWLevelMeterTestWindow::SetStreamDetector( ITAStreamDetector* pDetector )
{
	m_pDetector = pDetector;

	if( !pDetector )
		return;

	double dBlockTime = double( pDetector->GetBlocklength() ) / pDetector->GetSampleRate(); // Seconds
	if( pDetector )
		ui->graphicsView_LevelMeter->SetNumChannels( pDetector->GetNumberOfChannels() );

	if( !m_qAudioProcessTimer.isActive() )
	{
		m_qAudioProcessTimer.setInterval( int( dBlockTime * 1000.0f ) ); // Milliseconds
		m_qAudioProcessTimer.start();
	}

	if( !m_qRenderTimer.isActive() )
	{
		m_qRenderTimer.setInterval( 1000 / 25 ); // 25 Hz
		m_qRenderTimer.start();
	}
}

void CITAQtWLevelMeterTestWindow::Render()
{
	CITAQtWLevelMeter* pLM = ui->graphicsView_LevelMeter;

	if( m_pDetector )
	{
		m_pDetector->GetPeaks( m_vfPeaks, false );
		pLM->SetPeaks( m_vfPeaks );
		m_pDetector->GetRMSs( m_vfRMSs, true );
		pLM->SetRMSs( m_vfRMSs );
	}
}

void CITAQtWLevelMeterTestWindow::ProcessAudioStream()
{
	if( !m_pDetector )
		return;

	for( int i = 0; i < int( m_pDetector->GetNumberOfChannels() ); i++ )
	{
		const float* pfData = m_pDetector->GetBlockPointer( i, &m_siState );
	}

	m_pDetector->IncrementBlockPointer();

	m_siState.nSamples += m_pDetector->GetBlocklength();
	m_siState.dStreamTimeCode += m_qAudioProcessTimer.interval() / 1000.0f; // Milliseconds
}

void CITAQtWLevelMeterTestWindow::on_actionQuit_triggered()
{
	close();
}
