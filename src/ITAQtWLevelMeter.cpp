/*
 * ----------------------------------------------------------------
 *
 *		ITA acoustic visualization libs
 *      Licensed under Apache License, Version 2.0
 *
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2021
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAQtWLevelMeter.h>

#include <ITAException.h>
#include <ITANumericUtils.h>
#include <ITAConstants.h>

#include <cassert>

CITAQtWLevelMeter::CITAQtWLevelMeter( QWidget* parent /* = Q_NULLPTR */ )
	: QGraphicsView( parent )
	, m_dDynamicRangeDB( 60.0f )
	, m_dMaximumLevelDB( 6.0f )
	, m_bUpdateOnLevelChange( false )
{
	QGraphicsScene* pScene = new QGraphicsScene();
	pScene->setBackgroundBrush( QBrush( Qt::black, Qt::SolidPattern ) );

	setScene( pScene );

	connect( this, SIGNAL( LevelMetersChanged() ), this, SLOT( UpdateLevels() ) );
}

CITAQtWLevelMeter::~CITAQtWLevelMeter()
{

}

void CITAQtWLevelMeter::UpdateOnLevelChange( bool bYes )
{
	m_bUpdateOnLevelChange = bYes;
}

void CITAQtWLevelMeter::SetNumChannels( const int iNumChannels )
{
	if( iNumChannels < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "CITAQtWLevelMeter: Negative channel number given" );

	m_vfPeaks.resize( iNumChannels );
	m_vfRMSs.resize( iNumChannels );
	m_vfSensitivities.resize( iNumChannels );
	for( int i = 0; i < iNumChannels; i++ )
		m_vfSensitivities[ i ] = 1.0f;
}

int CITAQtWLevelMeter::GetNumChannels() const
{
	return int( m_vfPeaks.size() );
}

void CITAQtWLevelMeter::SetPeaks( const std::vector< float >& vfFactors )
{
	if( int( vfFactors.size() ) != GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number does not match number of available channels" );

	m_vfPeaks = vfFactors;

	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
};

void CITAQtWLevelMeter::SetRMSs( const std::vector< float >& vfRMSs )
{
	if( int( vfRMSs.size() ) != GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number does not match number of available channels" );

	m_vfRMSs = vfRMSs;

	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
};


void CITAQtWLevelMeter::SetPeak( const int iChannel, const float fFactor )
{
	if( iChannel > GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number exceeds number of available channels" );

	m_vfPeaks[ iChannel - 1 ] = fFactor;
	
	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
}

void CITAQtWLevelMeter::SetRMS( const int iChannel, const float fRMS )
{
	if( iChannel > GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number exceeds number of available channels" );

	m_vfRMSs[ iChannel - 1 ] = fRMS;

	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
}

void CITAQtWLevelMeter::SetSensitivity( const int iChannel, const float fSensitivity )
{
	if( iChannel > GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number exceeds number of available channels" );

	m_vfSensitivities[ iChannel - 1 ] = fSensitivity;

	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
}

void CITAQtWLevelMeter::SetSensitivities( const std::vector< float >& vfSensitivities )
{
	if( int( vfSensitivities.size() ) != GetNumChannels() )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given channel number does not match number of available channels" );

	m_vfSensitivities = vfSensitivities;

	if( m_bUpdateOnLevelChange )
		emit LevelMetersChanged();
}

void CITAQtWLevelMeter::UpdateLevels()
{
	scene()->clear();

	if( height() < 2 || width() < 2 )
		return;

	int iHeight = int( height() - 2 );
	int iNumElements = int( m_vfPeaks.size() );
	assert( iNumElements == m_vfRMSs.size() );
	assert( iNumElements == m_vfSensitivities.size() );

	for( int i = 0; i < iNumElements; i++ )
	{
		//const float& fPeak( m_vfFactors[ i ] );
		const float& fRMS( m_vfRMSs[ i ] );
		const float& fSensitivity( m_vfSensitivities[ i ] );
		const double dLevel = ratio_to_db20( double( std::fabs( fRMS * fSensitivity ) ) );

		QRect LevelMeterBackground( QPoint( i * 5, 0 ), QPoint( i * 5 + 4, iHeight ) );
		scene()->addRect( LevelMeterBackground, QPen(), QBrush( QColor( 232, 241, 250 ), Qt::SolidPattern ) );

		QBrush b( QBrush( QColor( 0, 84, 159 ), Qt::SolidPattern ) );
		int iInverseHeight = iHeight - 1; // quiet is default value
		if( dLevel > m_dMaximumLevelDB )
		{
			iInverseHeight = 1; // Panic clipping
			b.setColor( Qt::red );
		}
		else
		{
			iInverseHeight = std::max( 1, int( ( std::abs( dLevel ) ) ) );

			if( dLevel < m_dMaximumLevelDB - m_dDynamicRangeDB )
			{
				iInverseHeight = iHeight - 1; // No level
				b.setColor( Qt::darkGray );
			}
			else if( dLevel > 0.0f )
			{
				b.setColor( Qt::darkYellow ); // Warning clipping
			}
			else
			{
				b.setColor( QColor( 0, 84, 159 ) ); // Valid ranges
			}
		}
		
		assert( iInverseHeight < iHeight && iInverseHeight > 0);

		QRect LevelMeterActive( QPoint( i * 5, iInverseHeight ), QPoint( i * 5 + 4, iHeight ) );
		scene()->addRect( LevelMeterActive, QPen(), b );

		// Text
		QString sText;
		sText.sprintf( "%03.1f dB (%i)", dLevel, iInverseHeight );

		QFont font;
		font.setFamily( "Calibri" );
		font.setPointSize( 10 );

		QGraphicsTextItem* LevelText = new QGraphicsTextItem;
		LevelText->setPos( QPoint( iNumElements * 6 + 30, i * 20 ) );
		LevelText->setPlainText( sText );
		LevelText->setFont( font );
		LevelText->setDefaultTextColor( QColor( 232, 241, 250 ) );
		scene()->addItem( LevelText );
	}
}
