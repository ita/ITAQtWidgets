cmake_minimum_required( VERSION 2.9 )

project( ITAQtWidgets )

list( APPEND CMAKE_MODULE_PATH "$ENV{VISTA_CMAKE_COMMON}" )
include( VistaCommon )

set( CMAKE_AUTOMOC ON )


# dependencies
find_package( Qt5 REQUIRED COMPONENTS Core Gui Widgets ) # no vista cmake find_package

vista_use_package( VistaCoreLibs REQUIRED COMPONENTS VistaBase FIND_DEPENDENCIES )
vista_use_package( ITABase REQUIRED FIND_DEPENDENCIES )
vista_use_package( ITADataSources REQUIRED FIND_DEPENDENCIES )
vista_find_package( ITAGeo QUIET )
vista_find_package( ITADiffraction QUIET )


# includes
include_directories( "include" )


# files
set( ITAQtWHeader
	"include/ITAQtWDefinitions.h"
	"include/ITAQtWLevelMeter.h"
	)
set( ITAQtWSources
	"src/ITAQtWLevelMeter.cpp"
	)


# preprocessor
if( BUILD_SHARED_LIBS )
	add_definitions( -DITA_QT_WIDGETS_EXPORT )
else( )
	add_definitions( -DITA_QT_WIDGETS_STATIC )
endif( )

if( ITA_CORE_LIBS_BUILD_STATIC )
	add_definitions( -DITA_BASE_STATIC -DITA_DATA_SOURCES_STATIC )
endif( )

if( ITA_GEOMETRICAL_ACOUSTICS_BUILD_STATIC )
	add_definitions( -DITA_GEO_STATIC -DITA_DIFFRACTION_STATIC )
endif( )

if( ITA_VISTA_BUILD_STATIC )
	add_definitions( -DVISTABASE_STATIC -DVISTAMATH_STATIC -DVISTAASPECTS_STATIC -DVISTATOOLS_STATIC -DVISTAINTERPROCCOMM_STATIC )
endif( )


# linker
add_library( ITAQtWidgets ${ITAQtWHeader} ${ITAQtWSources} )
target_link_libraries( ITAQtWidgets ${VISTA_USE_PACKAGE_LIBRARIES} Qt5::Core Qt5::Gui Qt5::Widgets )


# configure
vista_configure_lib( ITAQtWidgets )
vista_install( ITAQtWidgets )
set( ITAQTWIDGETS_INCLUDE_OUTDIR "${CMAKE_CURRENT_SOURCE_DIR}/include" )
vista_create_cmake_configs( ITAQtWidgets )
vista_create_default_info_file( ITAQtWidgets )

set_property( TARGET ITAQtWidgets PROPERTY FOLDER "ITAAcousticVisualization" )


# tests
if( ITA_ACOUSTIC_VISUALIZATION_WITH_TESTS )
	set( ITAQTWIDGETS_COMMON_BUILD TRUE )
	add_subdirectory( "${CMAKE_CURRENT_SOURCE_DIR}/tests" )
endif()
